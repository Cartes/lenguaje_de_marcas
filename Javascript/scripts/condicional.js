function CalcularMayor()
{
	var n1Cadena,
		n1Entero,
		n2Cadena,
		n2Entero,
		resultado;

	n1Cadena = document.getElementById('numero1').value;	//con esto cogemos el numero
	n1Entero = parseInt (n1Cadena);   
	n2Cadena = document.getElementById('numero2').value;
	n2Entero = parseInt (n2Cadena);
	resultado = document.getElementById('resultado');
	if (n1Entero > n2Entero)	
		resultado.innerHTML +=
			"<br> El mayor es: " + n1Entero;
	else
		resultado.innerHTML +=
			"<br> El mayor es: " + n2Entero;
}

function CalcularDiasMes()
{
	var mesCadena,
		mes,
		diasMes,
		resultado;

	mesCadena = document.getElementById('numeroMes').value;	//con esto cogemos el numero
	mes = parseInt (mesCadena);

	switch(mes)
	{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12: diasMes = 31;
				 break;
		case 4:
		case 6:
		case 9:
		case 11: diasMes = 30;
				 break;
		case 2: diasMes = 28;
				 break;
		default:diasMes = 99;
				 break;   
	}

	resultado = document.getElementById('resultado');
	resultado.innerHTML +=
	"El mes: " + mes + " tiene " + diasMes + " dias ";
}	